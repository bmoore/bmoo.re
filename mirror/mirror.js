(function() {

  var updateClock = function() {
    var today = new Date();
    var clock = today.toDateString() + ' ' + today.toLocaleTimeString();
    document.getElementById('clock').innerHTML = clock;
    setTimeout(updateClock, 1000);
  };
  updateClock();

  var ajaxIt = function(url, success) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
      if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
          var data = JSON.parse(httpRequest.responseText);
          success(data);
        } else {
          console.log('busted');
        }
      }
    };
    httpRequest.open('GET', url, true);
    httpRequest.send();
  };

  var getWeather = function() {
    ajaxIt('weather.php', function(data) {
      document.getElementById('weather').innerHTML = data.hourly.summary;
    });
    setTimeout(getWeather,600000);
  }
  getWeather();

  var getTide = function() {
    ajaxIt('tide.php', function(data) {
      var svgTag = document.createElement('svg');
      d3plot(data);
      renderers['tides'] = {
        'func': d3plot,
        'data': data
      };
    });
    setTimeout(getTide,600000);
  }
  getTide();

  var getWiki = function() {
    var callback = 'wiki_callback_'+Math.round(100000*Math.random());
    var api_url = 'https://en.wikipedia.org/w/api.php?action=query&generator=random&grnnamespace=0&prop=extracts&exchars=200&format=json&callback='+callback;
    var wikiEl = document.getElementById('wiki')
    var script = document.createElement('script');

    window[callback] = function(data) {
      delete window[callback];
      document.body.removeChild(script);
      for (page_id in data.query.pages) break;
      page = data.query.pages[page_id];
      wikiEl.innerHTML = '<h3>'+page.title+'</h3>'+page.extract.substring(0,page.extract.indexOf('</p>'));
    }

    script.src = api_url;
    document.body.appendChild(script);
    setTimeout(getWiki, 900000);
  };
  getWiki();

  var d3plot = function(data) {
    var today = new Date();
    // set the dimensions and margins of the graph
    var margin = {top: 20, right: 20, bottom: 30, left: 50},
        aspect = 16/9
        width = (window.innerWidth/2) - margin.left - margin.right,
        height = (width / aspect) - margin.top - margin.bottom;

    // parse the date / time
    var parseTime = d3.timeParse("%Y-%m-%d %H:%M");

    // set the ranges
    var x = d3.scaleTime().range([0, width]);
    var y = d3.scaleLinear().range([height, 0]);

    // define the line
    var valueline = d3.line()
        .x(function(d) { return x(d.t); })
        .y(function(d) { return y(d.v); });

    document.getElementById('tide').innerHTML = '';
    var svg = d3.select("#tide").append('svg')
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");

    // format the data
    var preds = JSON.parse(JSON.stringify(data.predictions));
    preds.forEach(function(d) {
        d.t = parseTime(d.t);
        d.v = +d.v;
    });

    // Scale the range of the data
    x.domain(d3.extent(preds, function(d) { return d.t; }));
    y.domain(d3.extent(preds, function(d) { return d.v; }));

    // Add the valueline path.
    svg.append("path")
        .data([preds])
        .attr("class", "line")
        .attr("stroke", "#FFF")
        .attr("d", valueline);

    // Add the X Axis
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x).ticks(3));

    // Add the Y Axis
    svg.append("g")
        .call(d3.axisLeft(y).ticks(2));

    svg.append("line")
      .attr("x1", x(today))  //<<== change your code here
      .attr("y1", 0)
      .attr("x2", x(today))  //<<== and here
      .attr("y2", height)
      .style("stroke-dasharray", ("10,3"))

    var currentTideIndex = (today.getHours() * 10) + Math.round(today.getMinutes() / 6);
    var lastTideIndex = (today.getHours() * 10) + Math.round(today.getMinutes() / 6);
    var nextTideIndex = (today.getHours() * 10) + Math.round(today.getMinutes() / 6);
    console.log('curent tide', currentTideIndex, preds[currentTideIndex]);

    var svg2 = d3.select("#tide").append('svg')
        .attr("width", 10 + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");


    svg2.append('path')
      .attr('d', d3.symbol()
        .size(50)
        .type(d3.symbolCross)
      )
      .attr("transform", "translate(0,100)");

    // Add the X Axis
    var y2 = d3.scaleOrdinal().range([height, 0]).domain(['low', 'high']);
    svg2.append("g")
      .call(d3.axisLeft(y2).ticks(1));
  };

  var renderers = {};
  d3.select(window).on('resize', function() {
    for (i in renderers) {
      func = renderers[i].func;
      data = renderers[i].data;
      func(data);
    }
  });
})();
