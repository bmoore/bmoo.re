<?php
header('Content-Type: image/png');

$xkcd = json_decode(file_get_contents('http://xkcd.com/info.0.json'), TRUE);
$im = imagecreatefrompng($xkcd['img']);
if($im && imagefilter($im, IMG_FILTER_NEGATE))
{
    imagepng($im);
    imagedestroy($im);
}
